<?php


namespace App\Services;

use App\Entity\Point;

class ForelService
{
    public function getClustersByData(array $points): array
    {
        $clusters = [];
        $remainingPoints = $points;
        do {
            $cluster = $this->getCluster($remainingPoints);
            $clusters[] = $cluster;
            $remainingPoints = $this->diffPointsArrays($remainingPoints, $cluster);
        } while (!empty($remainingPoints));

        return $clusters;
    }

    protected function getCluster(array $points, ?float $radius = null): array
    {
        $hyperSphereCenter = $this->getHyperSphereCenter($points);
        if (null === $radius) {
            $radius = $this->getHyperSphereRadius($points, $hyperSphereCenter);
            $hyperSphereCenter = $this->getRandomPointInsideHyperSphere($hyperSphereCenter, $radius);
        }

        $pointsInsideHyperSphere = $this->getPointsInsideHyperSphere(
            $points,
            $hyperSphereCenter,
            0.9 * $radius
        );
        $newHyperSphereCenter = $this->getHyperSphereCenter($pointsInsideHyperSphere);
        if ($newHyperSphereCenter->isEqual($hyperSphereCenter)) {
            return $pointsInsideHyperSphere;
        }

        return $this->getCluster($pointsInsideHyperSphere, $radius);
    }

    protected function getHyperSphereCenter(array $points): Point
    {
        $hyperSphereCentre = new Point(count($points[0]->getCoordinates()));
        foreach ($points as $point) {
            foreach ($point->getCoordinates() as $key => $coordinate) {
                $hyperSphereCentre->setCoordinateByIndex(
                    $key,
                    $hyperSphereCentre->getCoordinateByIndex($key) + $coordinate
                );
            }
        }

        $pointsNumber = count($points);
        foreach ($hyperSphereCentre->getCoordinates() as $key => $coordinate) {
            $hyperSphereCentre->setCoordinateByIndex($key, $coordinate / $pointsNumber);
        }

        return $hyperSphereCentre;
    }

    protected function getHyperSphereRadius(
        array $points,
        Point $hyperSphereCenter
    ): float {
        $radiusSum = 0;
        $pointsNumber = count($points);
        foreach ($points as $point) {
            $underRoot = 0;
            foreach ($point->getCoordinates() as $key => $coordinate) {
                $underRoot += ($coordinate - $hyperSphereCenter->getCoordinateByIndex($key)) ** 2;
            }

            $radiusSum += $underRoot ** 0.5;
        }

        return $radiusSum / $pointsNumber;
    }

    protected function getRandomPointInsideHyperSphere(
        Point $hyperSphereCenter,
        int $hyperSphereRadius
    ): Point {
        $dimension = count($hyperSphereCenter->getCoordinates());
        $newPoint = new Point($dimension);
        foreach ($hyperSphereCenter->getCoordinates() as $key => $coordinate) {
            $newPoint->setCoordinateByIndex($key, $coordinate + rand(-$hyperSphereRadius, $hyperSphereRadius) / 1000);
        }

        return $newPoint;
    }

    protected function getPointsInsideHyperSphere(
        array $points,
        Point $hyperSphereCenter,
        int $hyperSphereRadius
    ): array {
        $result = [];
        $isInsideHyperSphere = true;
        foreach ($points as $point) {
            foreach ($point->getCoordinates() as $key => $coordinate) {
                $sphereCenterCoordinate = $hyperSphereCenter->getCoordinateByIndex($key);
                if ($sphereCenterCoordinate + $hyperSphereRadius < $coordinate ||
                    $sphereCenterCoordinate - $hyperSphereRadius > $coordinate) {
                    $isInsideHyperSphere = false;
                }
            }

            if ($isInsideHyperSphere) {
                $result[] = $point;
            }

            $isInsideHyperSphere = true;
        }

        return $result;
    }

    protected function diffPointsArrays(
        array $main,
        array $clustersPoints
    ): array {
        $result = [];
        $isEquals = false;
        foreach ($main as $point) {
            foreach ($clustersPoints as $clustersPoint) {
                if ($point->isEqual($clustersPoint)) {
                    $isEquals = true;
                }
            }

            if (!$isEquals) {
                $result[] = $point;
            }

            $isEquals = false;
        }

        return $result;
    }
}