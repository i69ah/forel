<?php

namespace App\Entity;

class Point
{
    /** @var int[] $coordinates  */
    private array $coordinates = [];

    public function __construct(int $dimension = 2)
    {
        for ($i = 0; $i < $dimension; $i++) {
            $this->coordinates[] = 0;
        }
    }

    public function getCoordinateByIndex(int $index): float
    {
        return $this->getCoordinates()[$index];
    }

    public function setCoordinateByIndex(int $index, float $value): void
    {
        $this->coordinates[$index] = $value;
    }

    /**
     * @return int[]
     */
    public function getCoordinates(): array
    {
        return $this->coordinates;
    }

    /**
     * @param int[] $coordinates
     */
    public function setCoordinates(array $coordinates): void
    {
        $this->coordinates = $coordinates;
    }

    public function isEqual(Point $point): bool
    {
        foreach ($point->getCoordinates() as $key => $coordinate) {
            $myCoordinate = ((int)($this->getCoordinateByIndex($key) * 10)) / 10;
            $notMyCoordinate = ((int)($coordinate * 10)) / 10;
            if ($myCoordinate !== $notMyCoordinate) {
                return false;
            }
        }

        return true;
    }
}