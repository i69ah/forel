<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Point;
use App\Services\ForelService;

class MainController extends AbstractController
{
    #[Route('/index', 'main_route')]
    public function index(Request $request): Response
    {
        $points = [
            new Point(2),
            new Point(2)
        ];
        if ($request->isMethod('post') && null !== $request->get('tableConfig')) {
            $tableConfig = $request->get('tableConfig');
            for ($i = 0; $i < $tableConfig['pointsNumber']; $i++) {
                $points[$i] = new Point($tableConfig['dimension']);
            }
        }

        return $this->render('main/index.html.twig', [
            'controller_name' => self::class,
            'points' => $points,
        ]);
    }

    #[Route('/processPoints', 'process_points')]
    public function processPoints(Request $request, ForelService $forelService): Response
    {
        if (!$request->isMethod('post') || null == $request->get('points')) {
            return new Response('Incorrect request', 400);
        }

        $points = [];
        $rawPoints = $request->get('points');
        foreach ($rawPoints as $rawPoint) {
            $point = new Point(count($rawPoint));
            foreach ($rawPoint as $axis => $coordinate) {
                $point->setCoordinateByIndex($axis, $coordinate);
            }

            $points[] = $point;
        }

        return $this->render('main/clusters.html.twig', [
            'clusters' => $forelService->getClustersByData($points),
        ]);
    }
}
